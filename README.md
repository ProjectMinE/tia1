# README #

### What is this repository for? ###

* This repository contains an Rscript to simulate a SKAT-O burden analysis
* The simulation was made to check the burden analysis as performed in Mackenzie et al., 2017, Neuron (http://dx.doi.org/10.1016/j.neuron.2017.07.025)

